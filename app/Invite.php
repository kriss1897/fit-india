<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    public function sender(){
		return $this->hasOne('App\User', 'sender_id', 'id');	
	}
	public function receiver(){
		return $this->hasOne('App\User', 'receiver_id', 'id');	
	}
	public function challange(){
		return $this->belongsTo('App\Challange', 'challange_id', 'id');	
	}
}
