<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

// Facades
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //php 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'title'=>'required|max:150',
            'type'=>'required|max:10',
        ]);
        if($request->type == 'video')
            $this->validate($request,[
                'file'=>'required|mimetypes:video/*'
            ]);
        else
            $this->validate($request,[
                'file'=>'required|mimetypes:image/*'
            ]);

        $user = Auth::user();
        $p = new Post;
        $p->title = $request->get('title');
        $p->description = $request->get('description');
        $p->type = $request->get('type');
        $p->file = $request->file('file')->store('files');
        $p->user_id = $user->id;
        $p->challange_id = $request->get('challange');
        $p->save();
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

    public function file(Post $post)
    {
        $path = $post->file;
        $file = Storage::get($path);
        $type = Storage::mimeType($path);
        $size = Storage::size($path);
        return response($file)->header('Content-Type',$type);
    }
}
