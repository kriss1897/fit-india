<?php

namespace App\Http\Controllers;

use App\Challange;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Http\File;

// Facades
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ChallangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $challanges = Challange::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|max:150',
            'type'=>'required|max:10',
            'category'=>'required|integer',
        ]);
        if($request->type == 'video')
            $this->validate($request,[
                'file'=>'required|mimetypes:video/*'
            ]);
        else
            $this->validate($request,[
                'file'=>'required|mimetypes:image/*'
            ]);

        $user = Auth::user();
        $c = new Challange;
        $c->title = $request->get('title');
        $c->description = $request->get('description');
        $c->category_id = $request->get('category');
        $c->user_id = $user->id;
        $c->save();
        $p = new Post;
        $p->title = $request->get('title');
        $p->description = $request->get('description');
        $p->type = $request->get('type');
        $p->file = $request->file('file')->store('files');
        $p->user_id = $user->id;
        $p->challange_id = $c->id;
        $p->save();
        $i = new \App\Invite;
        $i->sender_id = $user->id;
        $i->receiver_id = $user->id;
        $i->challange_id = $c->id;
        $i->save(); 
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Challange  $challange
     * @return \Illuminate\Http\Response
     */
    public function show(Challange $challange)
    {
        return view('challange.show')->with('challange',$challange);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Challange  $challange
     * @return \Illuminate\Http\Response
     */
    public function edit(Challange $challange)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Challange  $challange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Challange $challange)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Challange  $challange
     * @return \Illuminate\Http\Response
     */
    public function destroy(Challange $challange)
    {
        //
    }
}
