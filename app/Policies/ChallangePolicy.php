<?php

namespace App\Policies;

use App\User;
use App\Challange;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChallangePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the challange.
     *
     * @param  \App\User  $user
     * @param  \App\Challange  $challange
     * @return mixed
     */
    public function view(User $user, Challange $challange)
    {
        // $c = \App\Challange::find(7);
        return true;
    }

    /**
     * Determine whether the user can create challanges.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the challange.
     *
     * @param  \App\User  $user
     * @param  \App\Challange  $challange
     * @return mixed
     */
    public function update(User $user, Challange $challange)
    {
        //
    }

    /**
     * Determine whether the user can delete the challange.
     *
     * @param  \App\User  $user
     * @param  \App\Challange  $challange
     * @return mixed
     */
    public function delete(User $user, Challange $challange)
    {
        //
    }
}
