<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challange extends Model
{
	public function posts(){
		return $this->hasMany('App\Post', 'challange_id', 'id');	
	}

	public function category(){
		return $this->belongsTo('App\Category','category_id','id');
	}

	public function invites(){
		return $this->hasMany('App\Invite','challange_id','id');
	} 
}
