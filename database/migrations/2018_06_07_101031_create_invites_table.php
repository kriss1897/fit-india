<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites', function (Blueprint $table) {
            $table->integer('sender_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('receiver_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('challange_id')->references('id')->on('challanges')->onDelete('cascade');
            $table->primary(['receiver_id','challange_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invites');
    }
}
