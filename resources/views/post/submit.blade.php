@extends('layouts.app')

@section('content')
<div class="container">
    
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Submit Post</div>
            <div class="card-body">
                <p class='text-muted'>{{$challange->title}}</p>
                <form action="{{route('post.store')}}" method='post' enctype="multipart/form-data">
                    @csrf
                    <input name='challange' value="{{$challange->id}}" hidden>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input name='title' type="text" class="form-control" id="title" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="title">Description</label>
                        <textarea name='description' class="form-control" placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input name='file' type="file" class="custom-file-input">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="type" id="type1" value="video" required="" checked>
                          <label class="form-check-label" for="type1">Video</label>
                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name="type" id="type2" value="photo" required="">
                          <label class="form-check-label" for="type2">Photo</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit Entry</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection