@extends('layouts.app')

@section('content')
<div class="container">
    
    <br/>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3>Challanges <a class='btn btn-primary float-right' href="{{route('challange.create')}}">New Challange</a></h3>
            <hr/>
            <div class="card-columns">
            @foreach($challanges as $challange)
            @can('view',$challange)
            <div class="card">
                <div class="card-header"><a class='text-dark' href="{{route('challange.show',$challange)}}"><b>{{$challange->title}}</b></a><span class="badge badge-dark float-right">{{count($challange->posts)}} Posts</span></div>
                <div class="card-body">
                    <p class='card-text'> @php echo str_limit($challange->description,100,'...'); @endphp </p>
                </div>
            </div>
            <br/>
            @endcan
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
