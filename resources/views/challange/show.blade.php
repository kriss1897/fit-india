@extends('layouts.app')

@section('content')
<div class="container">
    <span class='badge badge-primary'>{{$challange->category->name}}</span>
    <h1>{{$challange->title}}</h1>
    <p class='text-muted'>{{$challange->description}}</p>
    <hr/>
    <h4>Invite User</h4>
    <form class="form-inline" method="post" action="{{route('invite.store')}}">
        @csrf
        <input hidden name='challange' value='{{$challange->id}}'>
        <label class="sr-only" for="inlineFormInputGroupUsername2">Email</label>
        <div class="input-group mb-2 mr-sm-2">
            <div class="input-group-prepend">
              <div class="input-group-text">User</div>
            </div>
            <input name='receiver' type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Email">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Invite</button>
    </form>
    <hr/>
    <h3>Posts
        @canPost($challange)
        <a class='btn btn-primary float-right' href="{{route('post.submit',$challange)}}">Submit Entry</a>
        @endcanPost
    </h3>

<div class='card-columns'>
    @foreach($challange->posts as $post)
    
    <div class='card'>
        @if($post->type == 'photo')
            <img class="card-img-top" src="{{route('post.file',$post)}}"/>
        @endif
        <div class='card-body'>
        <h4 class='card-title'>{{$post->title}}</h4>
        <small class='text-muted'>{{$post->user->name}}</small>
        
        @if($post->type == 'video')
        <video width="320" height="240" controls>
          <source src="{{route('post.file',$post)}}" type='video/mp4'>
        Your browser does not support the video tag.
        </video>
        @endif
        
    </div>
</div>
    @endforeach
    </div>
</div>
@endsection
