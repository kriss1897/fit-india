<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('challange/new',function(){
	$categories = \App\Category::all();
	return view('challange.new',['categories'=>$categories]);
})->name('challange.create');

Route::get('challange/{challange}/post',function(\App\Challange $challange){
	return view('post.submit',['challange'=>$challange]);
})->name('post.submit');

Route::apiResources([
    'challange' => 'ChallangeController',
    'post' => 'PostController',
    'invite' => 'InviteController'
]);

Route::get('post/{post}/file','PostController@file')->name('post.file');

Auth::routes();
